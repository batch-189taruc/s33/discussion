
// fetch is a way to retrieve data from an API. The .then function waits for the data to be fulfilled before it moves to another code.

// let posts = fetch('https://jsonplaceholder.typicode.com/posts')
	//fetch function returns a promise that is consumable using the .then syntax
// 			.then((response) => response.json())
// 			.then((json) => console.log(json))



// console.log(posts)


// async & await is the javascript ES6 equivalent of the .then syntax. It allows us to write more streamline/clean code while utilizing an API fetching functionality. It makes our code asynchronous.

async function fetchPosts(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	console.log(result)

	let json = await result.json()

	console.log(json)

}
fetchPosts()

// Get a single post from the API

// let post = fetch('https://jsonplaceholder.typicode.com/posts/10')//added "10" for specific object ID
// 			.then((response) => response.json())
// 			.then((data) => console.log(data))

// Create a single post
/*
	We can specify the method using a second object argument within the fetch function. Besides the method, we can also specify the headers and the body of the request.
*/
fetch('https://jsonplaceholder.typicode.com/posts/',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

// PUT 
/*
	1. ID of the post to be updated
	2. New body
*/
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated post'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


// let posts = fetch('https://jsonplaceholder.typicode.com/posts/100')
// 	//fetch function returns a promise that is consumable using the .then syntax
// 			.then((response) => response.json())
// 			.then((data) => console.log(data))

//DELETE
// delete a single post using the DELETE method
/*
	Make sure you specify:
	1. ID of post to be deleted
	2. No need for a header and body since we are deleting only
	3. 
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))


//FILTER
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((data) => console.log(data))

//request specific data, in the example below, fetch specific comments from specific post  using the /comments endpoint

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((data) => console.log(data))